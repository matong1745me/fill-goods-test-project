export const state = () => ({
  data: []
})

export const mutations = {
  setHistory (state, data) {
    state.data = data
  },
  addHistory (state, data) {
    state.data = [
      ...state.data,
      data
    ]
  },
  resetHistory (state) {
    state.data = []
  }
}
