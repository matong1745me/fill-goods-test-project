import axios from 'axios'
import apiConfig from '~/config/api'

const apiInstance = {
  api: null
}

export const createInstance = (context) => {
  // const { store } = context
  apiInstance.context = context
  const api = axios.create({
    baseURL: apiConfig.ENDPOINT
  })

  api.interceptors.request.use((config) => {
    // store.commit('loading/start')
    // if (store.state.auth.token) {
    //     config.headers = {
    //         Authorization: `Bearer ${store.state.auth.token}`,
    //     }
    // }
    return config
  })

  api.interceptors.response.use(
    (response) => {
      // store.commit('loading/end')
      return response
    },
    (error) => {
      // store.commit('loading/end')
      return error
    }
  )
  apiInstance.api = api
}

export default apiInstance
